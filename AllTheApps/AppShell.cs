﻿namespace AllTheApps;

public class AppShell : Shell
{
    public AppShell(MainPage Main, InstalledAppsPage InstalledApps)
    {
        Routing.RegisterRoute(nameof(InstalledAppsDetailPage), typeof(InstalledAppsDetailPage));
        Items.Add(new ShellContent { Title = "Main", Icon = ImageSource.FromFile("iconblank.png"), Content = Main });
        Items.Add(new ShellContent { Title = "InstalledApps", Icon = ImageSource.FromFile("iconlistdetail.png"), Content = InstalledApps });
    }
}
