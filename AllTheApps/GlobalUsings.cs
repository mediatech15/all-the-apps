﻿global using AllTheApps.Models;
global using AllTheApps.Services;
global using AllTheApps.ViewModels;
global using AllTheApps.Views;
global using CommunityToolkit.Maui;
global using CommunityToolkit.Maui.Markup;
global using CommunityToolkit.Mvvm.ComponentModel;
global using CommunityToolkit.Mvvm.Input;
global using System.Collections.ObjectModel;
global using System.Threading.Tasks;

