﻿namespace AllTheApps;

public static class MauiProgram
{
    public static MauiApp CreateMauiApp()
    {
        MauiAppBuilder builder = MauiApp.CreateBuilder();
        builder
            .UseMauiApp<App>()
            .UseMauiCommunityToolkit()
            .UseMauiCommunityToolkitMarkup()
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("FontAwesome6FreeBrands.otf", "FontAwesomeBrands");
                fonts.AddFont("FontAwesome6FreeRegular.otf", "FontAwesomeRegular");
                fonts.AddFont("FontAwesome6FreeSolid.otf", "FontAwesomeSolid");
                fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
            });

        builder.Services.AddTransient<AppShell>();

        builder.Services.AddSingleton<App>();

        builder.Services.AddTransient<MainPage, MainViewModel>();

        builder.Services.AddTransient<SampleDataService>();
        builder.Services.AddTransient<InstalledAppsDetailViewModel>();
        builder.Services.AddTransient<InstalledAppsDetailPage>();

        builder.Services.AddTransient<InstalledAppsPage, InstalledAppsViewModel>();

        return builder.Build();
    }
}
