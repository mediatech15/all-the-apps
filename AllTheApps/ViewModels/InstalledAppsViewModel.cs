﻿namespace AllTheApps.ViewModels;

public partial class InstalledAppsViewModel : BaseViewModel
{
    private readonly SampleDataService dataService;

    [ObservableProperty]
    private bool isRefreshing;

    [ObservableProperty]
    private ObservableCollection<SampleItem> items;

    public InstalledAppsViewModel(SampleDataService service)
    {
        dataService = service;
    }

    [RelayCommand]
    private async Task OnRefreshing()
    {
        IsRefreshing = true;

        try
        {
            await LoadDataAsync();
        }
        finally
        {
            IsRefreshing = false;
        }
    }

    [RelayCommand]
    public async Task LoadMore()
    {
        IEnumerable<SampleItem> items = await dataService.GetItems();

        foreach (SampleItem item in items)
        {
            Items.Add(item);
        }
    }

    public async Task LoadDataAsync()
    {
        Items = new ObservableCollection<SampleItem>(await dataService.GetItems());
    }

    [RelayCommand]
    private async Task GoToDetails(SampleItem item)
    {
        await Shell.Current.GoToAsync(nameof(InstalledAppsDetailPage), true, new Dictionary<string, object>
        {
            { "Item", item }
        });
    }
}
