﻿namespace AllTheApps.ViewModels;

[QueryProperty(nameof(Item), "Item")]
public partial class InstalledAppsDetailViewModel : BaseViewModel
{
    [ObservableProperty]
    private SampleItem? item;
}
