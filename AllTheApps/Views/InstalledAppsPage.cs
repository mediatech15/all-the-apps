﻿using AllTheApps.Resources.Styles;

namespace AllTheApps.Views;

public partial class InstalledAppsPage : ContentPage
{
    private readonly InstalledAppsViewModel ViewModel;

    public InstalledAppsPage(InstalledAppsViewModel viewModel)
    {
        BindingContext = ViewModel = viewModel;

        DataTemplate itemTemplate = new(() => new Frame()
        {
            GestureRecognizers = {
            new TapGestureRecognizer().BindCommand(source: ViewModel, path: nameof(InstalledAppsViewModel.GoToDetailsCommand), parameterPath: "."),
            },
            Content = new Label().Bind(Label.TextProperty, nameof(SampleItem.Title))
                             .FontSize(18)
                             .AppThemeBinding(Label.TextColorProperty, AppStyles.Get("Primary"), AppStyles.Get("Primary"))
        });

        Content = new RefreshView
        {
            Content = new CollectionView
            {
                RemainingItemsThreshold = 10,
            }.ItemTemplate(itemTemplate)
             .Bind(CollectionView.ItemsSourceProperty, nameof(InstalledAppsViewModel.Items))
             .Bind(CollectionView.RemainingItemsThresholdReachedCommandProperty, nameof(InstalledAppsViewModel.LoadMoreCommand))

        }.Bind(RefreshView.IsRefreshingProperty, nameof(InstalledAppsViewModel.IsRefreshing))
         .BindCommand(nameof(InstalledAppsViewModel.RefreshingCommand));
    }

    protected override async void OnNavigatedTo(NavigatedToEventArgs args)
    {
        base.OnNavigatedTo(args);

        await ViewModel.LoadDataAsync();
    }
}
