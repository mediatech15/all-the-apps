﻿namespace AllTheApps.Views;

public partial class MainPage : ContentPage
{
    [Obsolete]
    public MainPage(MainViewModel viewModel)
    {
        BindingContext = viewModel;

        Content = new Label
        {
            Text = $"Hello from the {viewModel.PageName}",
            VerticalOptions = LayoutOptions.CenterAndExpand,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            FontSize = 24
        };
    }
}
