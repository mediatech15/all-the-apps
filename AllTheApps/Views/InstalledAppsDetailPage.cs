﻿namespace AllTheApps.Views;

public partial class InstalledAppsDetailPage : ContentPage
{
    public InstalledAppsDetailPage(InstalledAppsDetailViewModel viewModel)
    {
        BindingContext = viewModel;

        Content = new ScrollView
        {
            Content = new VerticalStackLayout
            {
                Children = {
                    new Label().FontSize(20).Bind(Label.TextProperty, "Item.Title"),
                    new Label().Bind(Label.TextProperty, "Item.Description"),
                }
            }.Margin(12),
        };

    }
}
